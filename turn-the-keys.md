## Simplified list for _Turn the keys_
Remember to start turning from lower priorities if multiple TTKs modules are present!

### First, the right key
- Morse Code
- Wires
- Two Bits
- Button (The ,)
- Colo**u**r flash
- Round Keypad

### The left key
- Password
- Who's On First
- Crazy Talk
- Keypad
- Listening
- Orientation Cube

### Forbidden stuff:
#### Before the right key
- Semaphore
- Combination Lock
- Simon Says
- Astrology
- Switches
- Plumbing

#### Before the left key
- Maze
- Memory
- Complicated Wires
- Wire Sequences
- Cryptography
