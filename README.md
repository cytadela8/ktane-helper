# KTANE helper sheet

## [Manual pages](https://ktane.timwi.de)
[Translated Modules workshop page with manuals](https://steamcommunity.com/sharedfiles/filedetails/?id=850186070)
## Simplified list for _Turn the keys_
Remember to start turning from lower priorities if multiple TTKs modules are present!

### First, the right key
- Morse Code
- Wires
- Two Bits
- Button (The ,)
- Colo**u**r flash
- Round Keypad

### The left key
- Password
- Who's On First
- Crazy Talk
- Keypad
- Listening
- Orientation Cube

### Forbidden stuff:
#### Before the right key
- Semaphore
- Combination Lock
- Simon Says
- Astrology
- Switches
- Plumbing

#### Before the left key
- Maze
- Memory
- Complicated Wires
- Wire Sequences
- Cryptography
## Helpful general data
### Alphabet letter positions
```
1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26
A  B  C  D  E  F  G  H  I  J   K   L   M   N   O   P   Q   R   S   T   U   V   W   X   Y   Z
```
### NATO Phonetic Alphabet
```
A	Alpha
B	Bravo
C	Charlie
D	Delta
E	Echo
F	Foxtrot
G	Golf
H	Hotel
I	India
J	Juliet
K	Kilo
L	Lima
M	Mike
N	November
O	Oscar
P	Papa
Q	Quebec
R	Romeo
S	Sierra
T	Tango
U	Uniform
V	Victor
W	Whiskey
X	X-ray
Y	Yankee
Z	Zulu
```
## Modules by defuser engagement level
### Level 0
1. **Combination lock**
2. **Plumbing**
3. **Mystic Square**
4. **Blind alley**
5. **Battleship**
6. **Laundry**
- Additional remarks
*Need to solve as Xth module and submit carefully*
7. **Safety safe**
### Level 1
1. **Bitwise Operators**
- Information for the expert:
*Operator*
2. **skewed slots**
3. **Gamepad**
- Information for the expert:
*2 numbers from black display*
4. **Letter Keys**
5. **Emoji Math**
6. **Bitmaps**
7. **Alphabet**
8. **Chord qualities**
- Information for the expert:
*Notes pointed by yellow markers*
9. **Maze**
10. **Keypad**
11. **round keypad**
12. **Neutralization**
- Information for the expert:
*Solution color and volume*
13. **Point of Order**
14. **Text field**
- Information for the expert:
*The letter omnipresent on all subdisplays*
15. **Complicated buttons**
16. **Light cycle**
17. **Shape shift**
18. **Chess**
- Information for the expert:
*Click all digits, read out coordinates*
19. **Caesar cipher**
- Information for the expert:
*Letter sequence from black display*
20. **Foreign exchange rates**
- Information for the expert:
*Two 3-letter currency codes and amount*
21. **Square button**
22. **Rubik’s cube**
- Information for the expert:
*The colours of middle pieces on 3 visible walls*
### Level 2
1. **Astrology**
2. **RPSLS**
3. **Resistiors**
4. **Cheap checkout**
5. **Piano keys**
6. **Murder**
7. **Adventure game**
8. **Connection Check**
9. **Coordinates**
10. **Monsplode, fight!**
11. **Hexamazes**
12. **Fizzbuzz**
13. **Color math**
14. **Semaphore arrays**
15. **Symbolic password**
16. **Friendship**
